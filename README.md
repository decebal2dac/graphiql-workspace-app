GraphiQL Workspace – personal copy
==================================

An instance of [`crete-react-app`](https://github.com/facebookincubator/create-react-app) that runs a personal copy of [graphiql-workspace](https://github.com/OlegIlyenko/graphiql-workspace) (available at http://toolbox.sangria-graphql.org/graphiql).
Can be used as a docker image, which is automatically built with `.gitlab-ci.yml`.


Launching
---------

```bash
git clone https://gitlab.com/kachkaev/graphiql-workspace-app.git
cd graphiql-workspace-app
npm install

cp .env.dist .env
# configure default url, query and variables

# dev mode
npm start

# generate production build in /dist
npm run build
```

Inside docker on port 3500 (no need to clone the repo & npm install):
```bash
# start
docker run -it -d \
  --name graphiql \
  -p 3500:80 \
  registry.gitlab.com/kachkaev/graphiql-workspace-app:master \
&& docker logs -f graphiql

# stop
docker rm -f graphiql

# update
docker pull registry.gitlab.com/kachkaev/graphiql-workspace-app:master
```

If you organise your containers using [nginx-proxy](https://github.com/jwilder/nginx-proxy) and [docker-letsencrypt-nginx-proxy-companion](https://github.com/JrCs/docker-letsencrypt-nginx-proxy-companion), you can get a personal copy of graphiql workspace working at `https://graphiql.example.com` in just one command:

```bash
HOST=graphiql.example.com
LETSENCRYPT_EMAIL=letsencrypt@example.com

docker run -it -d \
  --name graphiql \
  -e VIRTUAL_HOST=${HOST},www.${HOST} \
  -e VIRTUAL_PORT=80 \
  -e VIRTUAL_NETWORK=web-proxy \
  -e LETSENCRYPT_HOST=${HOST},www.${HOST} \
  -e LETSENCRYPT_EMAIL=${LETSENCRYPT_EMAIL} \
  --network web-proxy \
  registry.gitlab.com/kachkaev/graphiql-workspace-app:master \
&& docker logs -f graphiql

```
