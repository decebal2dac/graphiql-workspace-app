FROM node:7.4.0

RUN npm install yarn -g

COPY ./ /var/www/graphiql

WORKDIR /var/www/graphiql

RUN cd /var/www/graphiql
RUN yarn install && yarn run build

EXPOSE 9000
CMD [ "yarn", "run", "start" ]

#from nginx:1.11.6-alpine

#COPY build /usr/share/nginx/html
#COPY docker/default.conf /etc/nginx/conf.d/default.conf

#EXPOSE 80
